import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:my_netflix/models/user.model.dart' as model;

class AuthMethods{
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  final googleSignIn = GoogleSignIn();

  GoogleSignInAccount? _user;

  GoogleSignInAccount get user => _user!;

  Future googleLogin() async {
    final googleUser = await googleSignIn.signIn();

    if(googleUser == null) return ;
    _user = googleUser;

    final googleAuth = await googleUser.authentication;

    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    await FirebaseAuth.instance.signInWithCredential(credential);
  }



  Future<void> signOut() async {
    await _auth.signOut();
  }

  //get user detail in model
  Future<model.User> getUserDetails() async {
    User currentUser = _auth.currentUser!;

    DocumentSnapshot snapshot = await _firestore.collection('users').doc(currentUser!.uid).get();

    return model.User.fromSnap(snapshot);
  }

  //Sign up user
  Future<String> signUpUser({
    required String email,
    required String password,
    required String username,
  }) async {
    String res = "Some error Occurred";
    try{
      if (email.isNotEmpty ||
          password.isNotEmpty ||
          username.isNotEmpty){
      }
      // Register with email and password
      UserCredential cred = await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );


      //add user to database
      model.User _user = model.User(
        username: username,
        uid: cred.user!.uid,
        email: email,
      );

      await _firestore.collection('users').doc(cred.user!.uid).set(_user.toJson());
      res = "success";
    }catch(err){
      res =  err.toString();
    }
    return res;
  }



  //sign in user
  Future<String> loginUser({
  required String email,
    required String password
}) async {
    String res = "Some error Occurred";
    try{
      if (email.isNotEmpty || password.isNotEmpty) {
        // logging in user with email and password
        await _auth.signInWithEmailAndPassword(
          email: email,
          password: password,
        );
        res = "success";
      } else {
        res = "Please enter all the fields";
      }
    }catch(err){
      res = err.toString();
    }
    return res;
  }


}