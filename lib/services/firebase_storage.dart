import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_storage/firebase_storage.dart';

class Storage {
  final firebase_storage.FirebaseStorage storage =
      firebase_storage.FirebaseStorage.instance;

  Future<List<String>> listFiles(section) async {
    firebase_storage.ListResult results = await storage.ref(section).listAll();
    final List<String> allFiles = [];

    // results.items.forEach((firebase_storage.Reference ref) {
    //   print('Found file: $ref');
    // });

    await Future.forEach<firebase_storage.Reference>(results.items,
        (file) async {
      final String fileUrl = await file.getDownloadURL();
      allFiles.add(fileUrl);
    });
    return allFiles;
  }
}
