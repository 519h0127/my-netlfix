import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RowOptions extends StatelessWidget {
  const RowOptions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Row(
      children: [
        Column(children: const [
          Icon(
            Icons.notifications_none_outlined,
            color: Colors.white,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            "Remind me",
            style: TextStyle(
                color: Colors.white, fontSize: 11),
          )
        ]),
        SizedBox(
          width: 30,
        ),
        Column(children: const [
          Icon(
            Icons.info_outline,
            color: Colors.white,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            "Info",
            style: TextStyle(
                color: Colors.white, fontSize: 11),
          )
        ]),
      ],
    );
  }
}
