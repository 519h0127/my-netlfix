import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_netflix/pages/home_page/components/represent_display.dart';
import 'package:my_netflix/pages/home_page/components/display_items.dart';
import 'package:my_netflix/pages/home_page/components/header.dart';
import 'package:my_netflix/pages/video_detail_page/video_detail_page.dart';
import 'package:cached_network_image/cached_network_image.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: getBody(),
    );
  }

  Widget getBody() {
    var size = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              width: size.height - 80,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //BACKGROUND-IMAGE
                  RepresentDisplay(),
                  SizedBox(
                    height: 10,
                  ),
                  //Option-Section
                  SizedBox(
                    height: 10,
                  ),

                  //Render List of Item
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      //------------------------MY LIST SECTION------------------------------
                      const Padding(
                        padding: EdgeInsets.only(left: 15, right: 15),
                        child: Text(
                          "My List",
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),

                      //DISPLAY SECTION
                      DisplayItems(section: 'myList'),
                      SizedBox(
                        height: 30,
                      ),

                      //------------------------POPULAR SECTION------------------------------
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: Text(
                          "Popular on Netflix",
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),

                      //DISPLAY SECTION
                       DisplayItems(section: 'popular'),
                      SizedBox(
                        height: 30,
                      ),

                      //------------------------TRENDING SECTION------------------------------
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: Text(
                          "Trending Now",
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),

                      //DISPLAY SECTION
                      DisplayItems(section: 'trending'),
                      SizedBox(
                        height: 30,
                      ),

                      //------------------------NETFLIX ORIGIN SECTION------------------------------
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: Text(
                          "Netflix Origins",
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),

                      //DISPLAY SECTION
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: StreamBuilder(
                                stream: FirebaseFirestore
                                    .instance
                                    .collection('items')
                                    .doc('movies')
                                    .collection('original')
                                    .snapshots(),
                                builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                                  if (!snapshot.hasData) {
                                    return const Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                    return Row(
                                      children: List.generate(
                                          snapshot.data.docs.length, (index) {
                                        return GestureDetector(
                                          onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (_) => VideoDetailPage(item: snapshot.data.docs[index], section: 'original',)));
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(right: 8),
                                            width: 165,
                                            height: 300,
                                            child: CachedNetworkImage(
                                              progressIndicatorBuilder:
                                                  (context, url, progress) =>
                                                  Center(
                                                    child: CircularProgressIndicator(
                                                      value: progress.progress,
                                                    ),
                                                  ),
                                              fit: BoxFit.cover,
                                              imageUrl: snapshot.data.docs[index]['img'],
                                            ),
                                          ),
                                        );
                                      }),
                                    );
                                })),
                      ),

                      SizedBox(
                        height: 30,
                      ),

                      //------------------------ANIME SECTION------------------------------
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: Text(
                          "Anime",
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),

                      //DISPLAY SECTION
                      DisplayItems(section: 'anime'),
                      SizedBox(
                        height: 30,
                      ),
                    ],
                  )
                ],
              ),
            ),

            //HEADER
            Header()
          ],
        ),
      ),
    );
  }
}
