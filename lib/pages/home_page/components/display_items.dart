import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../video_detail_page/video_detail_page.dart';

class DisplayItems extends StatefulWidget {
  final String section;

  const DisplayItems({Key? key, required this.section}) : super(key: key);

  @override
  State<DisplayItems> createState() => _DisplayItemsState();
}

class _DisplayItemsState extends State<DisplayItems> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Padding(
          padding: const EdgeInsets.only(left: 10),
          child: StreamBuilder(
              stream: FirebaseFirestore.instance
                  .collection('items')
                  .doc('movies')
                  .collection(widget.section)
                  .snapshots(),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                print('snap $snapshot');
                return Row(
                    children: List.generate(snapshot.data.docs.length, (index) {
                  print(snapshot.data.docs[index]);
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => VideoDetailPage(
                                    item: snapshot.data.docs[index], section: widget.section,
                                  )));
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 8),
                      width: 110,
                      height: 160,
                      child: CachedNetworkImage(
                        progressIndicatorBuilder: (context, url, progress) =>
                            Center(
                          child: CircularProgressIndicator(
                            value: progress.progress,
                          ),
                        ),
                        imageUrl: snapshot.data.docs[index]['img'],
                      ),
                    ),
                  );
                }));
              })),
    );
  }
}
