import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../video_detail_page/video_detail_page.dart';

class PlayButton extends StatelessWidget {
  final String section;
  const PlayButton({Key? key, required this.item, required this.section}) : super(key: key);
  final DocumentSnapshot item;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => VideoDetailPage(item: item, section: section)));
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(4)),
        child: Padding(
          padding: const EdgeInsets.only(
              right: 13, left: 8, top: 2, bottom: 2),
          child: Row(
            children: [
              Icon(
                Icons.play_arrow,
                color: Colors.black,
                size: 30,
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                "Play",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
