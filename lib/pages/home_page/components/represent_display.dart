import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_netflix/pages/home_page/components/play_button.dart';

import '../../../utils/size_config.dart';

class RepresentDisplay extends StatefulWidget {
  const RepresentDisplay({Key? key}) : super(key: key);

  @override
  State<RepresentDisplay> createState() => _RepresentDisplayState();
}

class _RepresentDisplayState extends State<RepresentDisplay> {
  int _currentPage = 0;
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  AnimatedContainer buildDot({int? index}) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      margin: EdgeInsets.only(right: 5),
      height: 6,
      width: _currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: _currentPage == index ? Colors.red : Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Stack(
      children: [
        Container(
          height: 400,
          child: StreamBuilder<Object>(
              stream: FirebaseFirestore.instance
                  .collection('items')
                  .doc('movies')
                  .collection('comingSoon')
                  .snapshots(),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (!snapshot.hasData) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return PageView(
                    controller: pageController,
                    onPageChanged: (value) {
                      setState(() {
                        _currentPage = value;
                      });
                    },
                    children: List.generate(snapshot.data.docs.length, (index) {
                      return Container(
                        child: Stack(
                          children: [
                            CachedNetworkImage(
                              progressIndicatorBuilder:
                                  (context, url, progress) => Center(
                                child: CircularProgressIndicator(
                                  value: progress.progress,
                                ),
                              ),
                              imageUrl: snapshot.data.docs[index].data()['img'],
                              fit: BoxFit.cover,
                              width: double.maxFinite,
                              height: double.maxFinite,
                            ),
                            Center(
                              child: CachedNetworkImage(
                                progressIndicatorBuilder:
                                    (context, url, progress) => Center(
                                  child: CircularProgressIndicator(
                                    value: progress.progress,
                                  ),
                                ),
                                imageUrl: snapshot.data.docs[index]
                                    .data()['title_img'],
                                width: 300,
                              ),
                            ),
                            Positioned(
                              bottom: 100,
                              left: 70,
                              child: Text(
                                snapshot.data.docs[index].data()['type'],
                                style: TextStyle(
                                    color: Colors.white.withOpacity(0.9),
                                    fontSize: 12,
                                    height: 1.4),
                              ),
                            ),
                            Positioned(
                              bottom: 30,
                              left: 165,
                              child: PlayButton(item: snapshot.data.docs[index], section: "comingSoon"),
                            ),
                          ],
                        ),
                      );
                    }));
              }),
        ),
        //Backgroun-Image title
        Container(
          height: 400,
          width: size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 30,
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 5),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: List.generate(
                          3,
                          (index) => buildDot(index: index),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
