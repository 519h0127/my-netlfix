import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_netflix/screens/profile/profile_screen.dart';

class ProfileButton extends StatelessWidget {
  const ProfileButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => ProfileScreen()
              )
          );
        },
        icon: Image.asset(
          "assets/images/profile.jpeg",
          width: 26,
          height: 26,
          fit: BoxFit.cover,
        ));
  }
}
