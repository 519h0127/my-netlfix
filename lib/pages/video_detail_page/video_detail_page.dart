import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_netflix/pages/home_page/components/header.dart';
import 'package:my_netflix/pages/video_detail_page/components/functions_list.dart';
import 'package:my_netflix/pages/video_detail_page/components/header_scroll_view.dart';
import 'package:my_netflix/pages/video_detail_page/components/options_scroll_view.dart';
import 'package:video_player/video_player.dart';



class VideoDetailPage extends StatefulWidget {
  final DocumentSnapshot item;
  final String section;
  const VideoDetailPage({Key? key, required this.item, required this.section}) : super(key: key);

  @override
  State<VideoDetailPage> createState() => _VideoDetailPageState();
}

class _VideoDetailPageState extends State<VideoDetailPage> {
  late VideoPlayerController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = VideoPlayerController.network(widget.item['video'])
      ..initialize().then((value) {
        setState(() {
          _controller.play();
        });
      });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: getAppBar(),
      ),
      body: getBody(),
    );
  }

  Widget getAppBar() {
    return AppBar(
      backgroundColor: Colors.black,
      elevation: 0,
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      actions: [
        IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.collections_bookmark,
              size: 28,
              color: Colors.white,
            )),
        IconButton(
            icon: Image.asset(
              "assets/images/profile.jpeg",
              fit: BoxFit.cover,
              width: 26,
              height: 26,
            ),
            onPressed: () {}),
      ],
    );
  }

  Widget getBody() {
    var size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      height: size.height,
      alignment: Alignment.center,
      child: Column(
        children: [
          //Video Screen
          Container(
            height: 220,
            child: Stack(
              children: [
                Container(
                  decoration:
                      BoxDecoration(color: Colors.grey.withOpacity(0.2)),
                  width: double.maxFinite,
                  child: Transform.scale(
                    scale: 5.4 / 5,
                    child: AspectRatio(
                        aspectRatio: 3.2 / 2, child: VideoPlayer(_controller)),
                  ),
                ),
                Center(
                  child: IconButton(
                    icon: Icon(
                      _controller.value.isPlaying ? null : Icons.play_arrow,
                      color: Colors.white,
                      size: 50,
                    ),
                    onPressed: () {
                      setState(() {
                        _controller.value.isPlaying
                            ? _controller.pause()
                            : _controller.play();
                      });
                    },
                  ),
                ),
                Positioned(
                  right: 5,
                  bottom: 20,
                  child: Container(
                    width: 25,
                    height: 25,
                    decoration: BoxDecoration(
                        color: Colors.black, shape: BoxShape.circle),
                    child: Center(
                      child: Icon(
                        Icons.volume_mute,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: size.height - 330,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.item['title'],
                      style: TextStyle(
                          height: 1.4,
                          fontSize: 25,
                          color: Colors.white.withOpacity(0.9),
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    HeaderScrollView(),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Watch Season 1 Now",
                      style: TextStyle(
                          color: Colors.white.withOpacity(0.9),
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          _controller.value.isPlaying
                              ? _controller.pause()
                              : _controller.play();
                        });
                      },
                      child: Container(
                        width: size.width,
                        height: 38,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(4)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              _controller.value.isPlaying
                                  ? Icons.pause
                                  : Icons.play_arrow,
                              color: Colors.black,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              _controller.value.isPlaying ? "Pause" : "Resume",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: size.width,
                      height: 38,
                      decoration:
                          BoxDecoration(color: Colors.grey.withOpacity(0.3)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.file_download,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Download",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "S1:E1 The Rise Nobunaga",
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 17,
                          color: Colors.white.withOpacity(0.9)),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: (size.width - 30) * 0.75,
                          child: Stack(
                            children: [
                              Container(
                                width: (size.width - 30) * 0.75,
                                height: 2.5,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.grey.withOpacity(0.5)),
                              ),
                              Container(
                                width: (size.width - 30) * 0.2,
                                height: 2.5,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.red.withOpacity(0.8)),
                              )
                            ],
                          ),
                        ),
                        Text(
                          "35 remaining",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.grey,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Considered a fool and unfit to lead, Nobunaga rises to power as the head of the Oda clan, spurring dissent among those in his family vying for control.",
                      style: TextStyle(
                          height: 1.4, color: Colors.white.withOpacity(0.9)),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Cast: Masayoshi Haneda, Masami Kosaka, Hideaki Ito... more",
                      style: TextStyle(
                          fontSize: 13,
                          height: 1.4,
                          color: Colors.grey.withOpacity(0.9)),
                    ),
                    FunctionsList(),
                    SizedBox(
                      height: 20,
                    ),
                    //Options Scrollviews
                    OptionScrollView(),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      "Season 1",
                      style: TextStyle(
                          fontSize: 17,
                          color: Colors.white.withOpacity(0.5),
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 15,
                    ),

                    StreamBuilder(
                        stream: FirebaseFirestore.instance
                            .collection('items')
                            .doc('movies')
                            .collection(widget.section)
                            .doc(widget.item.id)
                            .collection("episodes").snapshots(),
                        builder: (BuildContext context,
                            AsyncSnapshot<dynamic> snapshot) {
                          if (!snapshot.hasData) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          print("Detail ${snapshot != null}");
                          if(snapshot.data.docs.length > 3){
                            return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: List.generate(snapshot.data?.docs.length, (index) {
                                  return Padding(
                                    padding: const EdgeInsets.only(bottom: 20),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              width: (size.width - 30) * 0.85,
                                              height: 100,
                                              child: Row(
                                                children: [
                                                  Container(
                                                    width: 150,
                                                    child: Stack(
                                                      children: [
                                                        Container(
                                                          width: 150,
                                                          height: 90,
                                                          child: CachedNetworkImage(
                                                            progressIndicatorBuilder: (context, url, progress) =>
                                                                Center(
                                                                  child: CircularProgressIndicator(
                                                                    value: progress.progress,
                                                                  ),
                                                                ),
                                                            imageUrl: snapshot.data?.docs[index]['img'],
                                                          ),
                                                        ),
                                                        Container(
                                                          width: 150,
                                                          height: 90,
                                                          decoration: BoxDecoration(
                                                              color: Colors.black
                                                                  .withOpacity(0.3)),
                                                        ),
                                                        Positioned(
                                                          top: 30,
                                                          left: 57,
                                                          child: Container(
                                                            width: 38,
                                                            height: 38,
                                                            decoration: BoxDecoration(
                                                                shape: BoxShape.circle,
                                                                border: Border.all(
                                                                    width: 2,
                                                                    color:
                                                                    Colors.white),
                                                                color: Colors.black
                                                                    .withOpacity(0.4)),
                                                            child: Center(
                                                              child: Icon(
                                                                Icons.play_arrow,
                                                                color: Colors.white,
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    width: (size.width) * 0.42,
                                                    child: Padding(
                                                      padding: const EdgeInsets.only(
                                                          left: 10),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                        CrossAxisAlignment.start,
                                                        mainAxisAlignment:
                                                        MainAxisAlignment.center,
                                                        children: [
                                                          Text(
                                                            snapshot.data?.docs[index]['title'],
                                                            style: TextStyle(
                                                                fontSize: 15,
                                                                height: 1.3,
                                                                fontWeight:
                                                                FontWeight.w400,
                                                                color: Colors.white
                                                                    .withOpacity(0.9)),
                                                          ),
                                                          SizedBox(
                                                            height: 3,
                                                          ),
                                                          Text(
                                                            snapshot.data?.docs[index]['duration'],
                                                            style: TextStyle(
                                                                fontSize: 12,
                                                                color: Colors.white
                                                                    .withOpacity(0.5)),
                                                          ),
                                                          SizedBox(
                                                            height: 3,
                                                          ),
                                                          Text(
                                                            snapshot.data?.docs[index]['description'],
                                                            style: TextStyle(
                                                                fontSize: 12,
                                                                color: Colors.white
                                                                    .withOpacity(0.5)),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            Container(
                                              width: (size.width - 30) * 0.15,
                                              height: 100,
                                              child: Center(
                                                child: Icon(
                                                  Icons.file_download,
                                                  color: Colors.white.withOpacity(0.7),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  );
                                }));
                          }else{
                            return Text("");
                          }

                        }),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
