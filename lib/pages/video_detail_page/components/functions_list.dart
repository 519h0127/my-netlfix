import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';




class FunctionsList extends StatelessWidget {
  const FunctionsList({Key? key}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    List likesList = [
      {"icon": Icons.add, "text": "My List"},
      {"icon": Icons.thumb_up_alt_outlined, "text": "Rate"},
      {"icon": Icons.send_outlined, "text": "Share"}
    ];

    return Padding(
      padding: const EdgeInsets.only(left: 0,top: 10),
      child: Row(
        children: List.generate(likesList.length, (index) {
          return Padding(
            padding: const EdgeInsets.only(right: 50),
            child: Column(
              children: [
                Icon(
                  likesList[index]['icon'],
                  size: 25,
                  color: Colors.white.withOpacity(0.9),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(likesList[index]['text'],
                    style: TextStyle(
                        fontSize: 12,
                        height: 1.4,
                        color: Colors.grey.withOpacity(0.9)))
              ],
            ),
          );
        }),
      ),
    );
  }
}
