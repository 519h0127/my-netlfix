import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HeaderScrollView extends StatelessWidget {
  const HeaderScrollView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Row(
      children: [
        Text(
          "New",
          style: TextStyle(
              color: Colors.green,
              fontWeight: FontWeight.bold,
              fontSize: 15),
        ),
        SizedBox(
          width: 15,
        ),
        Text(
          "2021",
          style: TextStyle(
              fontSize: 15,
              color: Colors.white.withOpacity(0.5),
              fontWeight: FontWeight.w500),
        ),
        SizedBox(
          width: 15,
        ),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(2),
              color: Colors.white.withOpacity(0.2)),
          child: const Padding(
            padding: EdgeInsets.only(
                left: 6, right: 6, top: 4, bottom: 4),
            child: Text(
              "18+",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
        SizedBox(
          width: 15,
        ),
        Text(
          "1 Season",
          style: TextStyle(
              fontSize: 15,
              color: Colors.white.withOpacity(0.5),
              fontWeight: FontWeight.w500),
        ),
        SizedBox(
          width: 15,
        ),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(2),
              border: Border.all(
                  color: Colors.white.withOpacity(0.2),
                  width: 2)),
          child: const Padding(
            padding: EdgeInsets.only(
                left: 6, right: 6, top: 4, bottom: 4),
            child: Text(
              "HD",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
