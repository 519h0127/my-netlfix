import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class OptionScrollView extends StatefulWidget {
  const OptionScrollView({Key? key}) : super(key: key);

  @override
  State<OptionScrollView> createState() => _OptionScrollViewState();
}

class _OptionScrollViewState extends State<OptionScrollView> {
  int activeEpisode = 0;
  var episodesList = ["EPISODES", "TRAILERS & MORE", "MORE LIKE THIS"];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: List.generate(episodesList.length, (index) {
          return GestureDetector(
            onTap: () {
              setState(() {
                activeEpisode = index;
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 25),
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        border: Border(
                            top: BorderSide(
                                width: 4,
                                color: activeEpisode == index
                                    ? Colors.red
                                    .withOpacity(0.8)
                                    : Colors.transparent))),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 12),
                      child: Text(
                        episodesList[index],
                        style: TextStyle(
                            fontSize: 16,
                            color: activeEpisode == index
                                ? Colors.white.withOpacity(0.9)
                                : Colors.white.withOpacity(0.5),
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}
