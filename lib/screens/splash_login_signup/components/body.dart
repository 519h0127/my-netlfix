import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_netflix/screens/splash_login_signup/components/google_signIn_button.dart';
import 'package:my_netflix/ui/root.dart';
import 'package:shared_preferences/shared_preferences.dart';

// This is the best practice
import '../../../services/auth_methods.dart';
import '../../../utils/size_config.dart';
import '../../../utils/utils.dart';

class Body extends StatefulWidget {

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int _currentPage = 0;
  bool isChecked = false;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _isLoading = false;

  final TextEditingController _emailControllerLogin = TextEditingController();
  final TextEditingController _passwordControllerLogin =
      TextEditingController();


  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _usernameController.dispose();
    _emailControllerLogin.dispose();
    _passwordControllerLogin.dispose();
  }

  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );


  void signUpUser() async {
    // set loading to true
    setState(() {
      _isLoading = true;
    });

    String res = await AuthMethods().signUpUser(
      email: _emailController.text,
      password: _passwordController.text,
      username: _usernameController.text,
    );

    if (res == "success") {
      setState(() {
        _isLoading = false;
      });
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => const RootApp()));
      print("Sign up success");
    } else {
      setState(() {
        _isLoading = false;
      });
      // show the error
      showSnackBar(context, res);
    }
  }

  void loginUser() async {
    setState(() {
      _isLoading = true;
    });
    String res = await AuthMethods().loginUser(
        email: _emailController.text, password: _passwordController.text);
    if (res == 'success') {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => const RootApp()),
          (route) => false);

      setState(() {
        _isLoading = false;
      });
    } else {
      setState(() {
        _isLoading = false;
      });
      showSnackBar(context, res);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.4), BlendMode.dstATop),
          image: AssetImage("assets/images/netflixWall_1.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Container(
        child: SafeArea(
          child: Container(
            child: SizedBox(
              width: double.infinity,
              child: Container(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        "assets/images/netflixLogo.png",
                        height: 130,
                        width: 190,
                      ),
                      Expanded(
                          flex: 12,
                          child: PageView(
                            controller: pageController,
                            children: [LoginScreen(), SignUpScreen()],
                            onPageChanged: (value) {
                              setState(() {
                                _currentPage = value;
                              });
                            },
                          )),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(20)),
                          child: SingleChildScrollView(
                            child: Column(
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: List.generate(
                                    2,
                                    (index) => buildDot(index: index),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  AnimatedContainer buildDot({int? index}) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      margin: EdgeInsets.only(right: 5),
      height: 6,
      width: _currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: _currentPage == index ? Colors.red : Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }

  Widget LoginScreen() {

    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return Colors.grey;
    }

    return Padding(
        padding: EdgeInsets.only(left: 30, right: 30),
        child: Container(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 30,
                ),
                Text(
                  "Sign In",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 1, horizontal: 13),
                  decoration: BoxDecoration(color: HexColor("#343434")),
                  child: TextField(
                    style: TextStyle(color: Colors.grey),
                    decoration: InputDecoration(
                        hintText: 'Email',
                        border: InputBorder.none,
                        icon: Icon(
                          Icons.mail_outline,
                          color: Colors.grey,
                        ),
                        hintStyle: TextStyle(fontSize: 15, color: Colors.grey)),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 1, horizontal: 13),
                  decoration: BoxDecoration(color: HexColor("#343434")),
                  child: TextField(
                    style: TextStyle(color: Colors.grey),
                    decoration: InputDecoration(
                        hintText: 'Password',
                        border: InputBorder.none,
                        icon: Icon(
                          Icons.lock_outline,
                          color: Colors.grey,
                        ),
                        hintStyle: TextStyle(fontSize: 15, color: Colors.grey)),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                MaterialButton(
                  onPressed: loginUser,
                  minWidth: double.maxFinite,
                  color: HexColor("#e50913"),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: !_isLoading
                        ? const Text(
                            "Sign In",
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          )
                        : const CircularProgressIndicator(
                            color: Colors.white,
                          ),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 24.0,
                          width: 17.0,
                          child: Checkbox(
                            value: isChecked,
                            checkColor: Colors.black,
                            fillColor:
                                MaterialStateProperty.resolveWith(getColor),
                            onChanged: (bool? value) {
                              setState(() {
                                isChecked = value!;
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Align(
                          child: Text(
                            'Remember me',
                            style: TextStyle(
                                color: Colors.white70,
                                fontSize: 14,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
                    Align(
                      child: Text(
                        'Forgot Password?',
                        style: TextStyle(
                            color: Colors.white70,
                            fontSize: 14,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(children: [
                  Text(
                    "New to Netflix?",
                    style: TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.w500,
                        fontSize: 16),
                  ),
                  GestureDetector(
                    onTap: () async {
                      setState(() {
                        _currentPage = 1;
                      });
                      pageController.animateToPage(1,
                          curve: Cubic(0.25, 0.1, 0.25, 1.0),
                          duration: Duration(milliseconds: 1000));
                    },
                    child: Text(
                      " Sign Up",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 17),
                    ),
                  ),
                ]),
                SizedBox(
                  height: 80,
                ),
                GoogleButton(),
              ],
            ),
          ),
        ));
  }

  Widget SignUpScreen() {
    return Padding(
        padding: EdgeInsets.only(left: 30, right: 30),
        child: Container(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 30,
                ),
                Text(
                  "Sign Up",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30,
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 1, horizontal: 10),
                  decoration: BoxDecoration(color: HexColor("#343434")),
                  child: TextField(
                    controller: _usernameController,
                    style: TextStyle(color: Colors.grey),
                    decoration: InputDecoration(
                        hintText: 'Username',
                        border: InputBorder.none,
                        icon: Icon(
                          Icons.person,
                          color: Colors.grey,
                        ),
                        hintStyle: TextStyle(fontSize: 15, color: Colors.grey)),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 1, horizontal: 10),
                  decoration: BoxDecoration(color: HexColor("#343434")),
                  child: TextField(
                    controller: _emailController,
                    style: TextStyle(color: Colors.grey),
                    decoration: InputDecoration(
                        hintText: 'Email',
                        border: InputBorder.none,
                        icon: Icon(
                          Icons.mail_outline,
                          color: Colors.grey,
                        ),
                        hintStyle: TextStyle(fontSize: 15, color: Colors.grey)),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 1, horizontal: 10),
                  decoration: BoxDecoration(color: HexColor("#343434")),
                  child: TextField(
                    controller: _passwordController,
                    style: TextStyle(color: Colors.grey),
                    obscureText: true,
                    enableSuggestions: false,
                    autocorrect: false,
                    decoration: InputDecoration(
                        hintText: 'Password',
                        border: InputBorder.none,
                        icon: Icon(
                          Icons.lock_outline,
                          color: Colors.grey,
                        ),
                        hintStyle: TextStyle(fontSize: 15, color: Colors.grey)),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                MaterialButton(
                  onPressed: signUpUser,
                  minWidth: double.maxFinite,
                  color: HexColor("#e50913"),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      "Sign Up",
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Row(children: [
                  Text(
                    "Already a Netflix user?",
                    style: TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.w500,
                        fontSize: 16),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _currentPage = 0;
                      });
                      pageController.animateToPage(0,
                          curve: Cubic(0.25, 0.1, 0.25, 1.0),
                          duration: Duration(milliseconds: 1000));
                    },
                    child: Text(
                      " Sign in",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 17),
                    ),
                  ),
                ]),
                SizedBox(
                  height: 55,
                ),
                GoogleButton(),
              ],
            ),
          ),
        ));
  }
}
