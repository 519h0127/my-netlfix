import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'package:hexcolor/hexcolor.dart';
import 'package:my_netflix/screens/splash_login_signup/splash_login_signup_screen.dart';

import '../../../ui/root.dart';

final GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);

class LogOutButton extends StatefulWidget {
  LogOutButton({Key? key}) : super(key: key);

  @override
  State<LogOutButton> createState() => _LogOutButtonState();
}

class _LogOutButtonState extends State<LogOutButton> {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<void> signOut() async {
    await _auth.signOut();
    await _googleSignIn.signOut();
    setState(() {
    });
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: TextButton(
        style: TextButton.styleFrom(
          primary: Colors.white,
          padding: EdgeInsets.all(10),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          backgroundColor: Colors.red,
        ),
        onPressed: signOut,
        child: Row(
          children: [
            Icon(Icons.logout),
            SizedBox(width: 20),
            Expanded(child: Text("Log Out")),
            Icon(Icons.arrow_forward_ios),
          ],
        ),
      ),
    );
  }
}
