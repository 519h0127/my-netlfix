import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_netflix/screens/profile/components/log_out_button.dart';

import '../../../ui/root.dart';
import 'profile_menu.dart';
import 'profile_pic.dart';

class Body extends StatefulWidget {
  @override
  State<Body> createState() => _BodyState();
}


class _BodyState extends State<Body> {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.maxFinite,
      decoration: BoxDecoration(
        image: DecorationImage(
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.5), BlendMode.dstATop),
          image: AssetImage("assets/images/profile_background.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 20),
        child: Column(
          children: [
            SizedBox(
              height: 60,
            ),
            ProfilePic(),
            SizedBox(height: 20),
            Container(
              width: 371,
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
              decoration: BoxDecoration(
                  color: HexColor("#343434"),
                  borderRadius: BorderRadius.circular(5)),
              child: TextField(
                style: TextStyle(color: Colors.grey),
                decoration: InputDecoration(
                    hintText: 'Nhan',
                    border: InputBorder.none,
                    icon: Icon(
                      Icons.person,
                      color: Colors.grey,
                    ),
                    hintStyle: TextStyle(
                        fontSize: 17,
                        color: Colors.grey,
                        fontWeight: FontWeight.bold)),
              ),
            ),
            ProfileMenu(
              text: "Notifications",
              icon: Icon(Icons.notifications),
              press: () {},
            ),
            ProfileMenu(
              text: "Settings",
              icon: Icon(Icons.settings),
              press: () {},
            ),
            ProfileMenu(
              text: "Help Center",
              icon: Icon(Icons.help_outline_outlined),
              press: () {},
            ),
            LogOutButton(),
          ],
        ),
      ),
    );
  }
}
