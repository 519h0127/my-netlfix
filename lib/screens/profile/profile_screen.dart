import 'package:flutter/material.dart';


import 'components/body.dart';

class ProfileScreen extends StatelessWidget {
  static String routeName = "/profile";
  var MenuState = { "home", "favourite", "message", "profile" };
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.collections_bookmark,
                size: 28,
                color: Colors.white,
              )),
          IconButton(
              icon: Image.asset(
                "assets/images/profile.jpeg",
                fit: BoxFit.cover,
                width: 26,
                height: 26,
              ),
              onPressed: () {}),
        ],
      ),
      body: Body(),
      // bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.profile),
    );
  }


}
