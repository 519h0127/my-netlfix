import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:my_netflix/ui/root.dart';

import '../pages/home_page/home_page.dart';
import '../screens/profile/profile_screen.dart';
import '../screens/splash_login_signup/splash_login_signup_screen.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _RootAppState createState() => _RootAppState();
}

final GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email'
    ]
);

class _RootAppState extends State<MyApp> {
  int activeTab = 0;

  GoogleSignInAccount? _currentUser;

  @override
  void initState() {
    _googleSignIn.onCurrentUserChanged.listen((account) {
      setState(() {
        _currentUser = account;
      });
    });
    _googleSignIn.signInSilently();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      body: getBody(),
    );
  }

  Widget getBody() {
    GoogleSignInAccount? user = _currentUser;

    return StreamBuilder(
        stream: FirebaseAuth.instance.authStateChanges() ,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.active) {
            if (snapshot.hasData) {
              return RootApp();
            }else if(user != null){
              print("User $user");
              return RootApp();
            }
          }
          return SplashScreen();
        });
  }
}
