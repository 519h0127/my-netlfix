import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:my_netflix/pages/coming_soon_page/coming_soon_page.dart';
import 'package:my_netflix/pages/download_page.dart';
import 'package:my_netflix/pages/search_page.dart';

import '../pages/home_page/home_page.dart';

class RootApp extends StatefulWidget {
  const RootApp({Key? key}) : super(key: key);

  @override
  _RootAppState createState() => _RootAppState();
}

class _RootAppState extends State<RootApp> {
  int activeTab = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: bottomNavigation(),
      body: getBody(),
    );
  }
  Widget getBody(){
    return IndexedStack(
      index: activeTab,
      children: [
        HomePage(),
        ComingSoonPage(),
        SearchPage(),
        DownloadPage(),
      ],
    );
  }

  Widget bottomNavigation() {
    List items = [
      {"icon": AntDesign.home, "text": "Home"},
      {"icon": AntDesign.playcircleo, "text": "Coming Soon"},
      {"icon": AntDesign.search1, "text": "Search"},
      {"icon": AntDesign.download, "text": "Downloads"},
    ];



    return Container(
      height: 80,
      decoration: BoxDecoration(color: Colors.black),
      child: Padding(
        padding: const EdgeInsets.only(left: 20,right:20,top: 5 ),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(4, (index) {
              return GestureDetector(
                onTap: (){
                  setState(() {
                    activeTab = index;
                  });
                },
                child: Column(children: [
                  Icon(
                    items[index]['icon'],
                    color: Colors.white,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    items[index]['text'],
                    style: TextStyle(color: Colors.white, fontSize: 10),
                  )
                ]),
              );
            })
        ),
      ),
    );
  }
}
