#My-Netflix

##Cách Import dữ liệu dó sẵn vào database của người dùng:
1. Khởi tạo firebase của người dùng:
- Trường hợp không biết cách khởi tạo firebase hãy xem video hướng dẫn: https://www.youtube.com/watch?v=sz4slPFwEvs
- Sau khi làm theo các bước trong video, bạn được firebase cung cấp một file google-services.json
- Bên trong project, chúng ta đi đến thư mục android -> app, và sẽ có một file google-services.json có sẵn,
  người dùng chỉ cần xóa file gốc và ghi đè lên bằng file mới của mình.
- Thế là người dùng bây h đã có thể kiểm soát dữ liệu của ứng dụng trên firebase của mình.
2. Import tất cả các dữ liêu có sẵn vào firebase người dùng:
- Đầu tiên, chúng ta cần một package để hỗ trợ việc này,câu lệnh install package: npm i node-firestore-import-export
- Sau khi install, package sẽ được lưu ở thư mục npm trong máy tính,
  chúng ta cần copy link thư mục đó vào path bên trong environment variables (vd: C:\Users\NhanNguyen\AppData\Roaming\npm)
- Trên trang firebase, chúng ta đi vào mục project-settings -> Service accounts, bấm vào dòng chữ Generate new private key.
- Sau khi mặc định tải về file json mà firebase đã gợi í, để dễ nhất, chúng ta cần chuyển file json vừa tải vào
  cùng thư mục với file data.json trong thư mục assets/json bên trong project.
- Ở bên trong thư mục chứa hai file json, chúng ta mở cmd tại thư mục.
- Nhập lệnh: firestore-import -a {...tên file json vừa tải}.json -b data.json
3. Kiểm tra dữ liệu đã imported chưa trên firebase của người dùng.
- Nếu thành công người dùng có thể thấy các collection như items, users, và các collections con khác như là anime, myList... bên trong
- Nếu vẫn chưa thấy hoặc import thất bại, có thể xem video để tham khảo thêm về cách export/import: https://www.youtube.com/watch?v=gPzs6t3tQak

## Chạy ứng dụng
1. Trường hợp người dùng chạy bằng câu lệnh: flutter run
- Vì bên trong project có một số chương trình có các phiên bản hỗn hợp, nên chúng ta sẽ bị lỗi, để sửa lỗi này, người dùng
  chỉ cần thêm --no-sound-null-safety ở phía sau: flutter run --no-sound-null-safety.
2. Trường hợp chúng ta chạy bằng android studio và nhấn nút run 'main.dart'
- Vào Edit Configuration, ở mục additional run args, điền vào --no-sound-null-safety, sau đó apply và ok.